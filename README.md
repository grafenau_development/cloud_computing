# serverless-lambda-image-uploader

- Dieses Beispiel ladet Bilder in einen Bucket bei AWS. 


## Setup

- Node.js
- [Serverless](https://serverless.com) account 
- [AWS](https://aws.amazon.com) account

- Install and configure [Serverless](https://serverless.com/framework/docs/getting-started/)

## Deployment

- Clone repo
- run
```
npm i
serverless deploy
```
- follow the CLI instructions to setup the project

## Demo

follow this link to a [DEMO](https://cc2019imageuploader.s3.amazonaws.com/index.html)
